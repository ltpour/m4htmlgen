# M4HTMLGEN

## Summary

Generate static websites from m4 macros.

## Usage

Just run m4htmlgen. All m4 files under the directory tree, except for those in the *build* directory, are processed by m4 and the output for each of them, except for those in the *macros* directory, is written to corresponding files in the *build* directory. In this process the m4 extension is stripped, not replaced, so the initial extensions of source files should be of the form htm.m4, css.m4 and so on and so forth. Files without an m4 extension are copied verbatim. The m4 files in the *macros* directory contain the macro defitions used in generating the website. They are not processed by default and should be explicitly included where needed. The example in *test.htm.m4* shows one way to do this for all of these files without specifying each individually.