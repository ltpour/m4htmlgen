divert(-1)
define(`foreach',`ifelse(eval($#>2),1,`pushdef(`$1',`$3')$2`'popdef(`$1')`'ifelse(eval($#>3),1,`$0(`$1',`$2',shift(shift(shift($@))))')')')
foreach(`x', `ifelse(x,`',`',`include(x)')', patsubst(esyscmd(ls macros/*.m4),`
',`,'))dnl
divert(0)dnl
PG(`Page title',`sakura',`.postamble::after`,' .preamble::after { content: ""; clear: both; display: table; } .preamble`,' .postamble { font-size: 90%; margin: .2em; }',
DIV(`page',
`DIV(`preamble',
SPAN(`float: left;', I(`static/test.png', `preamble image', `200', `125'))
SPAN(`float: right;', P(`preamble')))
HR()
H(`1',`First heading')
UL(`unordered', `list')
H(`2',`Second heading')
OL(`ordered', `list')
P(`first paragraph`,' note that commas must be quoted!
second paragraph', `a class', `a style')
P(`third paragraph
fourth paragraph', `another class')
I(`static/test.png', `test image')
HR()
DIV(`postamble',
P(`postamble with A(`https://bitbucket.org/ltpour', `_blank', `a link')
and a date: esyscmd(`date +"%d.%m.%Y"')'))
`text outside paragraph''))
