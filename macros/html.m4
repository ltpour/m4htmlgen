divert(-1)

define(PG,`<!DOCTYPE html>
<html lang="en">
<head>
<title>$1</title>
<meta charset="utf-8">
ifelse(`$2',`',`',<link rel="stylesheet" type="text/css" href="static/$2.css">
)ifelse(`$3',`',`',<style>$3</style>
)</head>
<body>
$4
</body>
</html>')

dnl helper functions
define(`IT',`ifelse(`$#', `0',, `$#', `1',`LI(`$1')', `LI(`$1')IT(shift($@))')')

define(`LI',`ifelse(`$1',`', `', `<li>`$1'</li>')')

define(`PS',`ifelse(`$#', `0',, `$#', `1',`<p`'CL`'ST>`$1'</p>', `<p`'CL`'ST>`$1'</p>
`'PS(shift($@))')popdef(`CL')popdef(`ST')`'')

dnl html tags
define(`A',`<a href="`$1'"ifelse(`$2',`',`',` target="$2"')>`$3'</a>')

define(`BR',`<br>')

define(`DIV',`<div class="`$1'">
`$2'
</div>')

define(`EM',`<em>`$1'</em>')

define(`H',`<h`$1'>`$2'</h`$1'>')

define(`HR',`<hr>')

define(`I',`<img src="`$1'"ifelse(`$2',`',`',` alt="$2"')ifelse(`$3',`',`',` style="$3"')>')

define(`NAV',`<nav class="`$1'">
`$2'
</nav>')

define(`OL',`<ol>
IT($@)
</ol>')

define(`P',`pushdef(`CL',`ifelse(`$2',`',`',` class="$2"')')pushdef(`ST',`ifelse(`$3',`',`',` style="$3;"')')PS(patsubst(`$1',`
',`,'))`'')

define(`PRE',`<pre>`$1'</pre>')

define(`SEC',`<section class="`$1'">
`$2'
</section>')

define(`SOURCE',`<source src="`$1'"ifelse(`$2',`',`',` type="$2"')>')

define(`SPAN',`<span style="`$1'">
`$2'</span>')

define(`SUB',`<sub>`$1'</sub>')

define(`UL',`<ul>
IT($@)
</ul>')

define(`VIDEO',`<video ifelse(`$2',`',`',`width="$2"')ifelse(`$3',`',`',` height="$3"')ifelse(`$4',`nocontrols',`',` controls')`'ifelse(`$5',`autoplay',` autoplay',`')>
`$1'
</video>')

divert(0)dnl
