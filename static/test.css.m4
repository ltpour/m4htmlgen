/* Reset CSS via Eric Meyer (public domain)
http://meyerweb.com/eric/tools/css/reset/ 
*/

html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, embed, figure, figcaption, footer, header, hgroup,  menu, nav, output, ruby, section, summary, time, mark, audio, video {
    margin: 0; padding: 0; border: 0; font-size: 100%; font: inherit; vertical-align: baseline;}

article, aside, details, figcaption, figure, footer, header, hgroup, menu, nav, section { display: block; }
body { line-height: 1; }
ol, ul { list-style: none; }
blockquote, q { quotes: none; }
blockquote:before, blockquote:after, q:before, q:after { content: ''; content: none; }
table { border-collapse: collapse; border-spacing: 0; }

*, *:before, *:after { -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box;
}

divert(-1)
/* COLOURS */

define(`C1',`#d8bfd8')
define(`C2',`#dfcddf')
define(`C3',`#202020')
define(`C4',`#404040')
define(`C5',`#606060')

divert(0)dnl
/* LINKS */

a:link {
    color: #0000FF;
    text-decoration: none;
}
a:visited {
    color: #006600;
    text-decoration: none;
}
a:hover {
    color: #990000;
    text-decoration: underline;
}
a:active {
    color: #CC0000;
    text-decoration: none;
} 

a:link.titlelink {
    color: #dfcddf;
}
a:visited.titlelink {
    color: #dfcddf;
}
a:hover.titlelink {
    color: #dfcddf;
}
a:active.titlelink {
    color: #dfcddf;
} 

a {
    border:none;
}

/* BODY */

body {
    margin:0;
    padding:0;
    border:0;
    background-color:C1;
    font-size:100%;
}

/* HEADERS */

h1, h2, h3, h4 {
    font-family:"Palatino Linotype", Palatino, Palladio, "URW Palladio L", "Book Antiqua", Baskerville, "Bookman Old Style", "Bitstream Charter", "Nimbus Roman No9 L", Garamond, "Apple Garamond", "ITC Garamond Narrow", "New Century Schoolbook", "Century Schoolbook", "Century Schoolbook L", Georgia, serif;
    font-variant-caps: small-caps;
    font-weight: bold;
    text-align:center;
}  

h1 {
    background-color:C3;
    color:C2;
    font-size:2.5em;        
    line-height:1.25em;
    margin-top:1.5em;
    margin-bottom:1.5em;
}

@media only screen and (min-width: 40em) {
    h1 {
	margin-top:1em;
    }
}

h2 {
    color:C3;
    font-size:1.875em;
    margin-top:1em;    
    margin-bottom:1em;
}

h3 {
    color:C4;
    font-family:eb_garamond12_regular, "Palatino Linotype", Palatino, Palladio, "URW Palladio L", "Book Antiqua", Baskerville, "Bookman Old Style", "Bitstream Charter", "Nimbus Roman No9 L", Garamond, "Apple Garamond", "ITC Garamond Narrow", "New Century Schoolbook", "Century Schoolbook", "Century Schoolbook L", Georgia, serif;
    font-size:1.25em;
    margin-top:1em;
    margin-bottom:1em;
}

h4 {
    color:C5;
    font-size:1em;
    margin-top: 1.5em;
}

img {
    display: block;
    border:5px double C4;
    max-width:100%;
    margin-left: auto;
    margin-right: auto;
}

/* PARAGRAPHS */

p {
    font-family:"Palatino Linotype", Palatino, Palladio, "URW Palladio L", "Book Antiqua", Baskerville, "Bookman Old Style", "Bitstream Charter", "Nimbus Roman No9 L", Garamond, "Apple Garamond", "ITC Garamond Narrow", "New Century Schoolbook", "Century Schoolbook", "Century Schoolbook L", Georgia, serif;
    font-size:1em;
    margin-left:15%;
    margin-right:15%;
    margin-top:0.5em;
    margin-bottom:0.5em;
    text-indent:2em;
    text-align:justify;
    line-height:1.5em;
}

/* LISTS */

ol, ul {
    font-size:1em;
    margin-left:22%;
    margin-right:15%;
    margin-top:1.5em;
    margin-bottom:1.5em;
    text-indent:2em;
    text-align:justify;
    line-height:1.5em;
    list-style-type:disc;
}

ol li, ul li {
    font-family:"Palatino Linotype", Palatino, Palladio, "URW Palladio L", "Book Antiqua", Baskerville, "Bookman Old Style", "Bitstream Charter", "Nimbus Roman No9 L", Garamond, "Apple Garamond", "ITC Garamond Narrow", "New Century Schoolbook", "Century Schoolbook", "Century Schoolbook L", Georgia, serif;
    margin-left:0em;
    margin-top: 0em;
    padding-left:3em;
    text-indent:-2.5em;
}

/* SPECIAL AREAS */

div.page {
    background-color:C1;
    width:90%;
    min-width:20em;
    margin-left: auto;
    margin-right: auto;
    margin-top: 2em;
    margin-bottom: 1.5em;
    padding-left:0.5em;
    padding-right:0.5em;
    padding-bottom:0;
    padding-top:0;
    border:5px solid C4;
}
